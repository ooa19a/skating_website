## Skating Website

### I used the following technologies for this website 

* HTML
* CSS
* JavaScript
* Bootstrap

|**website screenshot**| **website screenshot** |**website screenshot**|
|-----|----|----|
|[![first image](img/scrnshot/toppage.png)](img/scrnshot/toppage.png) |[![second image](img/scrnshot/second_page.png)](img/scrnshot/second_page.png) |[![third image](img/scrnshot/first_modal.png)](img/scrnshot/first_modal.png)|
|[![footer image](img/scrnshot/footer.png)](img/scrnshot/footer.png)|[![modal footer image](img/scrnshot/modal_footer.png)](img/scrnshot/modal_footer.png)| 


#### Website Video

[![modal footer image](img/scrnshot/modal_footer.png)](img/website_video.mp4)
